﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SAC
{
    public class consulta
    {
//-----------------------------------------------------------------------------------------------------------

        public MySqlCommand ejecutar_consulta(String consulta, MySqlConnection con)
        {
            MySqlCommand comando;
            try
            {
                comando = new MySqlCommand(consulta, con);
            }
            catch (MySqlException)
            {
                comando = null;
            }
            return comando;
        }
//-------------------------------------------------------------------------------------------------------------

        //public MySqlCommand ejecutar_consultau(String consulta, MySqlConnection con)
        //{
        //    MySqlCommand comando;
        //    try
        //    {
        //        comando = new MySqlCommand(consulta, con);
        //        comando.Parameters.AddWithValue("@user", us);
        //        comando.Parameters.AddWithValue("@pass", pas);
        //    }
        //    catch (MySqlException)
        //    {

        //        comando = null;
        //    }
        //    return comando;
        //}
    }
}