﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SAC
{
    public partial class Index : System.Web.UI.Page
    {

        Metodos objme = new Metodos();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            txtFecha.Text = Calendar1.SelectedDate.ToString("yyyy-MM-dd");
            Calendar1.Visible = true;
        }

        public void Envio()
        {
            //try
            //{
                string Hora = "";
                Hora = idHora.Text + ":" + idMinutos.Text;

                objme.Agregarcita(txtcedula.Text,txtFecha.Text, Hora, txtTelefono.Text);
            string script = @"<script type='text/javascript'>

            alert('Cita exitosa ');
            </script>";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);

            //}
            //catch
            //{
            //string script = @"<script type='text/javascript'>

            //alert('No se han completado todos los campos requeridos devidamente');
            //</script>";
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);
            //}
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Envio();
        }
    }
}